package reporthandler.presentation;

import java.awt.Desktop;
import java.io.Console;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import reporthandler.logic.Component;
import reporthandler.logic.Logic;
import reporthandler.logic.Report;

/**
 *
 * @author CHJOMEI2
 * 
 * method readInt() proudly copied from M. Bontognali(ExMan_Package_Interface)
 */

public class CLI {
    private Logic logic;
    private Scanner input = new Scanner(System.in);
    
    public CLI (Logic logic){
        this.logic = logic;        
    }
    
    public void work(){
        int option;      
        
        String[] mainMenu = {"1. Search order",
                            "2. Validate reports",
                            "3. Enter metadata",
                            "4. Show temp metadata",
                            "5. Generate UID",
                            "6. Help",
                            "7. Search metadata",
                            "0. Exit"
                        };             
        
        //readCredentials();
        showMenu(mainMenu);
        do {
            option = readInt(0,mainMenu.length);
            switch(option){
                case 1: {searchOrder();break;}
                case 2: {searchReports();break;}
                case 3: {enterMetadata("","");break;}
                case 4: {showTempMetadata();break;}                
                case 5: {generateUID();break;}
                case 6: {help();showMenu(mainMenu);break;}                
                case 7: {searchMetadata();break;}
                case 0: {exit();break;}                
            }
        } while(option != 0);
        
    }    
    
    
    private void showMenu(String[] menu){
        System.out.println("\n\n----REPORT HANDLER IQC [1.0]----");
        System.out.println("Choose an option:");
        for (String m:menu){
                System.out.println(m);
            }        
        System.out.println("\nYour option is: ");
        }
    
           
    private void searchOrder(){
        System.out.println("\n\n________________________________");
        System.out.println("Please enter a ordernumber:");
        
        logic.searchOrder(readOrderNumber());
        
        System.out.println("\nSelect new option:");        
    }
    
    private void searchReports(){
        System.out.println("\n\n________________________________");
        System.out.println("Please enter a ordernumber:");        
        String ordernumber = readOrderNumber();
        
        logic.getReports(ordernumber).forEach(file->vailidateReport(file,ordernumber));              
        
        System.out.println("\nSelect new option: ");
    };
    
    
    private void vailidateReport(String filepath, String ordernumber){
        int option =0;
        System.out.println("\n\n________________________________");
        System.out.println("Approve "+filepath+" as report?");
        showReport(filepath);
        System.out.println(
                "\t0: This is not a report --> ignore file\n"
                + "\t1: Approve --> Enter metadata and save report\n"
                + "\t2: Decline --> inform supplier(is not yet implemented)");
                
        switch(option = readInt(0,2) ){            
                case 0: {System.out.println("File has been ignored");break;}
                case 1: {enterMetadata(filepath,ordernumber);break;}                
                case 2: {System.out.println("Report has been declined");break;}
                }                
    }
        
    private void showReport(String path){       
        try {
            Desktop desktop = Desktop.getDesktop();
            File report = new File(path);
            desktop.open(report);
        } catch (IOException ex) {
            System.out.println("Couldn't open report"+path);
        }
    }   
        
      
    private void enterMetadata(String url, String ordernumber){
        String materialnumber="";
        System.out.println("\n\n________________________________");     
        System.out.println("Enter metadata of this report");       
        
        if (url == ""){        
            System.out.println("URL-address: ");
            url = input.next();
        }
        
        if (ordernumber == ""){
            System.out.println("Ordernumber: ");
            ordernumber = input.next();
        }
        
        if (materialnumber == ""){
            System.out.println("Materialnumber: ");
            materialnumber = input.next();
        }
        
        reporthandler.logic.Report report = new Report(url,materialnumber,ordernumber);
        logic.addReport(report);
        
        System.out.println("Add component to this report? 0->NO 1->YES");
        while (readInt(0, 1) != 0) {
            
            input.reset();
            System.out.println("Serialnumber: ");
            String serialnumber = input.next();           
            
            System.out.println("Batchnumber: ");
            String batch = input.next();
                        
            report.addCompononent(new Component(serialnumber,batch));
            
            System.out.println("Add another component to this report? 0->NO 1->YES");
        }
       
        saveMetadata(report);
        
        System.out.println("Select new option:");
     
    }
        
    private void saveMetadata(Report report){
        System.out.println("saving metadata in DB...");
        logic.saveMetadata(report);        
    }
    
    private void searchMetadata(){
        List<Report> reports = new ArrayList<>();
        System.out.println("\n\n________________________________");
        System.out.println("Enter keyword:");
        String keyword = input.next();
        
        System.out.println("searching metadata from DB...");       
        reports = logic.searchMetadata(keyword);
        reports.forEach(r->System.out.println(r));
        
        System.out.println("Open reports? 0->NO 1->YES");
        if (readInt(0, 1)==1){
            reports.forEach(r->showReport(r.getUrl()));
        }
        
        System.out.println("\nSelect new option:");       
    }
    
    private void showTempMetadata(){
        System.out.println("Showing saved metadata...");
        
        logic.showTempMetadata();
        
        System.out.println("\nSelect new option");
    }
    
    private void generateUID(){
        System.out.println("Generating UID is not yet implmented");
    }
        
    
    private void help(){
        System.out.println("Its always a pleasure to help: ");
        
    }
    
    private void exit(){
        System.out.println("Ademessi");
    }
    
    private void  readCredentials(){
        //source to hide password in cmd: https://www.javatpoint.com/java-console-readpassword-method
        Console con = System.console();      
        System.out.println("\n\n________________________________");
        System.out.println("Login IQC-Mailaccount");   
        if(con == null){      
            System.out.print("No console available. Please run code on cmd.");   
            return;   
        }        
        else{
        System.out.println("Adress: ");        
        String address = readAddress();        
        System.out.println("Password: ");
        char[] ch=con.readPassword(); 
        String password = String.valueOf(ch);
        
        logic.checkLogin(address, password);
        }
        
    }
    
    
    
    
    private String readAddress(){
        String scan="";        
        while (input.hasNext()){
            scan = input.next();
            scan = scan.toLowerCase();
            if (scan.contains("@")){                
                break;
            }
            else{
                System.out.println("Please enter a mailadress");
                input.reset();
            } 
        }
        return scan; 
    }     
    
    
    private String readOrderNumber(){
        String scan="";
        while (input.hasNext()){
            scan = input.next();            
            if (scan.length()==10){                
                break;
            }
            else{
                System.out.println("Please enter a valid ordernumber");
                input.reset();
            } 
        }        
        return scan;
    }
    
    
    private int readInt(int min, int max){
        int scan = 0;
        while (input.hasNext()){
            if (input.hasNextInt()){
                scan = input.nextInt();
                    if (scan >= min && scan <= max) {
                        break;
                    }
                    else {
                            System.out.println("Enter a number in the valid range!");
                    }   
            }
            else{
                System.out.println("Enter a number!");
                input.next();
                }           
            }
        return scan;               
        }
    
}
    
   
