package reporthandler.logic;

import java.util.ArrayList;
import java.util.List;
import reporthandler.data.Database;
import reporthandler.data.Mailbox;


/**
 *
 * @author CHJOMEI2
 */
public class ReportHandling implements Logic{
    private Database database;
    private Mailbox mailbox;
    private Container container = new Container();
    private ArrayList <String> tmpReports = new ArrayList<>();
    
    
    public ReportHandling(Mailbox mailbox,Database database)  {
        this.mailbox = mailbox;
        this.database = database;
    }   
    
    
    public boolean checkLogin(String address, String password){
        //mailbox.setAddress(address);
        //mailbox.setPassword(password);
        System.out.println("Validating access...");
        mailbox.connect();
        mailbox.getFolder();
        mailbox.disconnect();
        System.out.println("Login OK");
        
    return true;
    }
    
    public void searchOrder(String ordernumber){
        mailbox.connect();
        mailbox.getFolder();
        System.out.println("Found messages: "+mailbox.searchMessage(ordernumber));
        mailbox.disconnect();                
    }
    
    
    public ArrayList <String> getReports(String ordernumber){
        mailbox.connect();
        mailbox.getFolder();        
        tmpReports = mailbox.getAttachments(ordernumber);        
        mailbox.disconnect();
        return tmpReports;
    }
    
    public void saveMetadata(Report report){  
        int keyProductionOrder;
        int keyUrl;         
               
        keyProductionOrder = database.save("INSERT INTO productionorder VALUES (NULL,'"+report.getOrdernumber()+"','"+report.getMaterialnumber()+"')");
        keyUrl = database.save("INSERT INTO report VALUES (NULL,'"+report.getUrl().replace("\\","\\\\")+"',"+keyProductionOrder+")");  
        
        if (report.getComponents().isEmpty()){
            database.save("INSERT INTO deliveredcomponent VALUES (NULL,'"+null+"','"+null+"',"+keyProductionOrder+")");  
        }
        else {
            report.getComponents().forEach(c->database.save("INSERT INTO deliveredcomponent VALUES (NULL,'"+c.getSerialnumber()+"','"+c.getBatch()+"',"+keyProductionOrder+")"));
            
            
        }
    }
    
    
    public ArrayList<Report> searchMetadata(String keyword){          
        return database.load("select SerialNr, MaterialNr, BatchNr, ProductionOrder, Url from deliveredcomponent\n" +
"           inner join productionorder on deliveredcomponent.ProductionOrderID = productionorder.ProductionOrderID\n" +
"           inner join report on productionorder.ProductionOrderID = report.ProductionOrderID\n" +
"           where SerialNr = '"+keyword+"' or ProductionOrder = '"+keyword+"' or BatchNr = '"+keyword+"' or MaterialNr = '"+keyword+"';");        
    }
        
    
    public void showTempMetadata(){        
        System.out.println(container.toString());
    }
    
        
    public void addReport(Report report){
        container.addReport(report);
    }
    
    
}
