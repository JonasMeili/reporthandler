package reporthandler.logic;

/**
 *
 * @author CHJOMEI2
 */
public class Component {
    private String serialnumber;    
    private String batch;
    
    public Component(String serialnumber, String batch){
        this.serialnumber = serialnumber;
        this.batch = batch;        
    }
    
    public String getSerialnumber(){
        return serialnumber;
    }
    
    public String getBatch(){
        return batch;
    }

    @Override
    public String toString() {
        return "Component{" + "serialnumber=" + serialnumber + ", batch=" + batch + '}';
    }

    
    
    
        
    
}
