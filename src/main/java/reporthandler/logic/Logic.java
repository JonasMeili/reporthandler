package reporthandler.logic;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author CHJOMEI2
 * 
 */
public interface Logic {
    boolean checkLogin(String address, String password);
    void searchOrder(String ordernumber);
    ArrayList<String> getReports(String ordernumber);
    void saveMetadata(Report report);
    ArrayList<Report> searchMetadata(String keyword);
    void showTempMetadata();
    void addReport(Report report);     
}
