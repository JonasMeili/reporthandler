package reporthandler.logic;

import java.util.ArrayList;


/**
 *
 * @author CHJOMEI2
 */
public class Report {
    private String url;
    private String ordernumber;
    private String materialnumber;
    private ArrayList <Component> components;  
    
    public Report(String url, String materialnumber, String ordernumber){
        this.url = url;
        this.ordernumber = ordernumber;   
        this.materialnumber = materialnumber;
        components = new ArrayList<>();
    }
    
    
    public String getUrl(){
        return url;
    }
    
    public String getOrdernumber(){
        return ordernumber;
    }    
    
    public String getMaterialnumber(){
        return materialnumber;
    }
       
    public void addCompononent(Component component){        
        components.add(component);
    }
    
    public Component getComponent(int index){
        return components.get(index);
    }
    
    public ArrayList<Component> getComponents(){
        return components;
    }

    @Override
    public String toString() {
        return "Report{" + "url=" + url + ", ordernumber=" + ordernumber + ", materialnumber=" + materialnumber + ", components=" + components + '}';
    }

    
}
