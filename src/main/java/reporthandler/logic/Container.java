package reporthandler.logic;

import java.util.ArrayList;

/**
 *
 * @author CHJOMEI2
 */
public class Container { 
    private ArrayList<Report> reports = new ArrayList<>();
      
    public void addReport(Report report){
        reports.add(report);
    }
    
    public Report getReport(int index){
        return reports.get(index);
    }
       

    @Override
    public String toString() {
        return "Container{" + "reports=" + reports + '}';
    }

    
    
}
