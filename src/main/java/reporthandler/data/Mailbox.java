package reporthandler.data;

import java.util.ArrayList;

/**
 *
 * @author CHJOMEI2
 */
public interface Mailbox{    
    void connect();
    void setAddress (String address);
    void setPassword(String password);
    void getFolder();
    void disconnect();
    int searchMessage(String keyword);
    ArrayList<String> getAttachments(String keyword);   
}
