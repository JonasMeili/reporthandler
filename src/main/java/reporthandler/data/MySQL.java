/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reporthandler.data;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import reporthandler.logic.Component;
import reporthandler.logic.Report;



/**
 *
 * @author CHJOMEI2
 */
public class MySQL implements Database{    
    private String protocol = "jdbc:mysql";
    private String host = "localhost";
    private String port = "3306";
    private String database = "iqc_reporthandling";
    private String options = "useSSL=false&"
            + "allowPublicKeyRetrieval=true&"
            + "serverTimezone=UTC";
    private String url = protocol+"://"+host+":"+port+"/"+database+"?"+options;
    
    private String user;
    private String password;
    
    private Connection connection = null;
    private Statement statement = null;
    private ResultSet data = null;    
    
    public MySQL (String user,String password){
        this.user = user;
        this.password = password;    
    }
       
    
    public int save(String query){
        ResultSet rs;
        int key=0;
        try{
            connection = DriverManager.getConnection(this.url,this.user,this.password);
            statement = connection.createStatement();
            statement.executeUpdate(query,Statement.RETURN_GENERATED_KEYS);
            rs =statement.getGeneratedKeys();
            if (rs != null && rs.next()) {
                key = rs.getInt(1);
            }
            System.out.println("->succesful");
        }
        catch(Exception e){
            System.out.println("->failed");
            e.printStackTrace();
        }
        return key;    
        
    }
    
    public ArrayList<Report> load(String query){
        ArrayList<Report> reports = new ArrayList<>();
        try{            
            connection = DriverManager.getConnection(this.url,this.user,this.password);
            statement = connection.createStatement();
            
            data=statement.executeQuery(query);
            while (data.next()){
                Report report = new Report(data.getString("URL"),data.getString("MaterialNr"),data.getString("ProductionOrder"));
                Component component = new Component(data.getString("SerialNr"),data.getString("BatchNr"));
                report.addCompononent(component);
                
                reports.add(report);
            }
            if (reports.isEmpty()){
                System.out.println("->no reports found");
            }
            else{
                System.out.println("->succesful");
            }            
        }
        catch(Exception e){
            System.out.println("->failed");
            e.printStackTrace();
        }       
    return reports;         
    }    
}
