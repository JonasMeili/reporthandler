package reporthandler.data;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties; 
import javax.mail.Address;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.NoSuchProviderException;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.MimeBodyPart;
import javax.mail.search.SearchTerm;

/**
 *
 * @author CHJOMEI2
 */
public class Outlook implements Mailbox{
    private String address;
    private String password;
    private String host = "outlook.office365.com";
    private String port = "993";
    private String protocol = "imap";
    private String saveDirectory = "C:\\Users\\chjomei2\\Desktop\\Attachment";
    private Folder folderInbox;
    private Store store;
    private Message[] messages;
    private ArrayList<String>tmpReports = new ArrayList<>();   
    private Properties properties = new Properties();
       

    public Outlook(String address, String password){
        this.address = address;
        this.password = password;
    }
    
    public void setAddress (String address){
        this.address = address;
    }
    
    public String getAddress(){
        return address;
    }
    
    public void setPassword(String password){
        this.password = password;
    }
    
    
    public ArrayList<String> getTmpReports(){
        return tmpReports;
    }
   
    
    public void connect(){       
        //server setting
        properties.put(String.format("mail.%s.host", protocol), host);
        properties.put(String.format("mail.%s.port", protocol), port);
       
        // SSL setting
        properties.setProperty(String.format("mail.%s.socketFactory.class", protocol),"javax.net.ssl.SSLSocketFactory");
        properties.setProperty(String.format("mail.%s.socketFactory.fallback", protocol),"false");
        properties.setProperty(String.format("mail.%s.socketFactory.port", protocol),String.valueOf(port));
 
        //Session session = Session.getDefaultInstance(properties);
        Session session = Session.getInstance(properties, null);
        
        try {
            // connects to the message store
            store = session.getStore(protocol);
            store.connect(address, password);               
 
        } catch (NoSuchProviderException ex) {
            System.out.println("No provider.");
            ex.printStackTrace();
        } catch (MessagingException ex) {
            System.out.println("Could not connect to the message store.");
            ex.printStackTrace();
        }  
                
    }
    
    public void getFolder(){
        try {            
            folderInbox = store.getFolder("Inbox");
            folderInbox.open(Folder.READ_ONLY);
 
        } catch (NoSuchProviderException ex) {
            System.out.println("No provider.");
            ex.printStackTrace();
        } catch (MessagingException ex) {
            System.out.println("Could not connect to the message store.");
            ex.printStackTrace();
        }  
        
    }
    
    
    public void disconnect(){    
        try {            
            folderInbox.close(false);
            store.close();
            //System.out.println("disconnected");
 
        } catch (NoSuchProviderException ex) {
            System.out.println("No provider.");
            ex.printStackTrace();
        } catch (MessagingException ex) {
            System.out.println("Could not disconnect  from the message store.");
            ex.printStackTrace();
        }  
        
      
    }
    
    public int searchMessage(String keyword){
        int sumMessages=0;
        SearchTerm searchCondition = new SearchTerm() {
                @Override
                public boolean match(Message message) {
                    try {
                        
                        if (message.getSubject().contains(keyword)) {
                            return true;
                        }
                    } catch (MessagingException ex) {
                        System.out.println("Connection error");
                        ex.printStackTrace();
                    }
                    return false;
                }
            };        
        
        // performs search through the folder
        try{
            System.out.println("Searching messages...");
            messages = folderInbox.search(searchCondition);            
            sumMessages = messages.length;
        }
        catch (MessagingException ex) {
            System.out.println("Search couldn't be completed.");
            ex.printStackTrace();
            }

    return sumMessages;
    }
    
    public ArrayList<String> getAttachments(String keyword){
        
        try{
            searchMessage(keyword);           
            
            for (int i = 0; i < messages.length; i++) {
                System.out.println("Loading attachments of message #"+(i+1)+"...");
                Message message = messages[i];
                Address[] fromAddress = message.getFrom();
                String from = fromAddress[0].toString();
                String subject = message.getSubject();
                String sentDate = message.getSentDate().toString();
 
                String contentType = message.getContentType();
                
 
                if (contentType.contains("multipart")) {
                    // content may contain attachments
                    Multipart multiPart = (Multipart) message.getContent();
                    int numberOfParts = multiPart.getCount();
                    for (int partCount = 0; partCount < numberOfParts; partCount++) {
                        MimeBodyPart part = (MimeBodyPart) multiPart.getBodyPart(partCount);
                        if (Part.ATTACHMENT.equalsIgnoreCase(part.getDisposition())) {
                            // this part is attachment
                            String fileUrl = saveDirectory + File.separator + part.getFileName();
                            
                            if (!tmpReports.contains(fileUrl) & fileUrl.endsWith(".pdf")){
                                tmpReports.add(fileUrl);
                                part.saveFile(fileUrl);
                                System.out.println(fileUrl); 
                            }
                                                                              
                        }
                    }
 
                   
                }
                
                
                // print out details of each message
                System.out.println("Message details#" + (i + 1) + ":");
                System.out.println("\t From: " + from);
                System.out.println("\t Subject: " + subject);
                System.out.println("\t Sent Date: " + sentDate);
                
                
            }
            
            
            
             } catch (NoSuchProviderException ex) {
            System.out.println("No provider found.");
            ex.printStackTrace();
        } catch (MessagingException ex) {
            System.out.println("Could not connect to the message store");
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return tmpReports;
    }
    
    
}
