
package reporthandler.data;

import java.util.ArrayList;
import reporthandler.logic.Report;

/**
 *
 * @author CHJOMEI2
 */
public interface Database{   
    int save(String query);
    ArrayList<Report> load(String query);       
}
