/*
 * This app logs into a mailaccount and loads attachments to a specific folder.
 * The saved attachment can be validated and enhanced with metadata and saved into a database. 
 * Implented by Jonas Meili for ABB POWER GRIDS SWITZERLAND AG
 */

import reporthandler.data.Database;
import reporthandler.data.Mailbox;
import reporthandler.logic.Logic;
import reporthandler.logic.ReportHandling;
/**
 *
 * @author CHJOMEI2
 */
public class ReportHandler {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //Data layer:
        //Mailbox mailbox = new reporthandler_iqc.data.Outlook("iqc.abb@hotmail.com","report_iqc");
        Mailbox mailbox = new reporthandler.data.Outlook("CHINQS\\info.qs@ch.abb.com","MJLrWwUmVET934up");        
        Database database = new reporthandler.data.MySQL("root","root");
        
        
        //Logic layer:
        Logic logic = new ReportHandling(mailbox,database);
        
        
        //Presentatzion layer:
        new reporthandler.presentation.CLI(logic).work();
    }
    
}
